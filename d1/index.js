// setup the dependencies
const express = require ('express');
const mongoose = require ('mongoose');
// this allows us to use all the routes defined in 'taskRoute.js'
const cors = require('cors');

// routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')

// server setup
const app = express();
const port = 4000;
// allows all resources/origin to access our backend application
app.use(cors())


/*
app.use(cors(corsOptions()))

let corsOptions = {
	origin: ['http://localhost:3000', 'http://localhost:8000']
}
*/
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// defines the '/user'
app.use('/api/user', userRoutes)
app.use('/api/course', courseRoutes)

// database connection
mongoose.connect("mongodb+srv://steventroy:steventroy@wdc028-course-booking.alyii.mongodb.net/batch-144-database?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", ()=> console.log ("we're connected to the cloud database"))

// Routes (base uri for task route)
// allows all the task routes created in the taskRoute.js file to use "/tasks" route



app.listen(port, () => console.log(`now listening to port ${port}`))

